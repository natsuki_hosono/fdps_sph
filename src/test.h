#pragma once

template <class ThisPtcl> void CheckConservativeVariables(const PS::ParticleSystem<ThisPtcl>& sph_system, const PS::F64 time){
	PS::F64vec Mom_loc = 0;  //total momentum
	PS::F64    Eng_loc = 0;  //total enegry
	PS::F64    EngKin_loc = 0;  //kinetic enegry
	PS::F64    EngPot_loc = 0;  //potential enegry
	PS::F64    EngInt_loc = 0;  //internal enegry
	PS::F64    Mass_loc = 0; //total mass
	for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
		Mom_loc += sph_system[i].vel * sph_system[i].mass;
		Eng_loc += (sph_system[i].eng + 0.5 * sph_system[i].vel * sph_system[i].vel + 0.5 * sph_system[i].pot) * sph_system[i].mass;
		EngKin_loc += 0.5 * sph_system[i].vel * sph_system[i].vel * sph_system[i].mass;
		EngInt_loc += sph_system[i].eng * sph_system[i].mass;
		EngPot_loc += 0.5 * sph_system[i].pot * sph_system[i].mass;
		Mass_loc += sph_system[i].mass;
	}
	PS::Comm::barrier();
	PS::F64vec Mom  = PS::Comm::getSum(Mom_loc);
	PS::F64 Eng  = PS::Comm::getSum(Eng_loc);
	PS::F64 Mass = PS::Comm::getSum(Mass_loc);
	PS::F64 EngKin  = PS::Comm::getSum(EngKin_loc);
	PS::F64 EngInt  = PS::Comm::getSum(EngInt_loc);
	PS::F64 EngPot  = PS::Comm::getSum(EngPot_loc);
	if(PS::Comm::getRank() == 0){
		printf("Mass: %.16e\n", Mass);
		printf("Eng : %.16e\n", Eng);
		printf("Momx: %.16e\n", Mom.x);
		printf("Momy: %.16e\n", Mom.y);
		#ifndef PARTICLE_SIMULATOR_TWO_DIMENSION
		printf("Momz: %.16e\n", Mom.z);
		#endif
		#if 1
		std::ofstream file;
		file.open("check.txt", std::ios::app);
		file << time << "\t" << Mass << "\t" << Mom << "\t" << EngKin << "\t" << EngInt << "\t" << EngPot << "\t" << EngKin + EngInt + EngPot << std::endl;
		file.close();
		#endif
	}
}

