#pragma once

/*
*/

namespace EoS{
	//Boltzman const.
	const double kB = 1.38064852e-23;
	//reduced Planck const.
	const double hbar = 1.054571800e-34;
	//pi
	const double pi = atan(1.0) * 4.0;
	//gas const;
	const double Rbar = 8.3144598;
	//Avogadro constant
	const double NA = 6.0221409e+23;
	//room temp.
	const double T0 = 298.0;

	//////////////////
	//abstract class
	//////////////////
	template <typename type> class EoS_t{
		public:
		EoS_t(){
			return ;
		}
		/* virtual */ ~EoS_t(){
			return ;
		}
		virtual type Pressure   (const type dens, const type eng) const = 0;
		virtual type SoundSpeed (const type dens, const type eng) const = 0;
		virtual type Phase      (const type dens, const type eng) const{
			return +0.0/0.0;
		}
		virtual type Temperature(const type dens, const type eng) const{
			return +0.0/0.0;
		}
	};
	//////////////////
	//EoSs
	//////////////////
	template <typename type> class IdealGas : public EoS_t<type>{
		const type hcr;//heat capacity ratio;
		public:
		IdealGas(const type _hcr) : hcr(_hcr){
		}
		inline type Pressure(const type dens, const type eng) const{
			return (hcr - 1.0) * dens * eng;
		}
		inline type SoundSpeed(const type dens, const type eng) const{
			return sqrt(hcr * (hcr - 1.0) * eng);
		}
		inline type HeatCapacityRatio() const{
			return hcr;
		}
	};
	template <typename type> class Tillotson : public EoS_t<type>{
		type rho0, a, b, A, B, u0, alpha, beta, uiv, ucv;
		inline type P_co(const type dens, const type eng) const{
			const type eta = dens / rho0;
			const type mu  = eta - 1.0;
			return (a + b / (eng / u0 / eta / eta + 1.0)) * dens * eng + A * mu + B * mu * mu;
		}
		inline type P_ex(const type dens, const type eng) const{
			const type eta = dens / rho0;
			const type mu  = eta - 1.0;
			return a * dens * eng + (b * dens * eng / (eng / u0 / eta / eta + 1.0) + A * mu * exp(- alpha * (1.0 / eta - 1.0))) * exp(- beta * (1.0 / eta - 1.0) * (1.0 / eta - 1.0));
		}
		inline type dPdrho(const type rho, const type u) const{
			const type drho = 0.0001;
			return (Pressure(rho + drho, u) - Pressure(rho - drho, u)) / (2.0 * drho);
		}
		inline type dPdu(const type rho, const type u) const{
			const type du = 0.0001;
			return (Pressure(rho, u + du) - Pressure(rho, u - du)) / (2.0 * du);
		}
		public:
		Tillotson(const type a_rho0, const type a_u0, const type a_uiv, const type a_ucv, const type a_A, const type a_B, const type a_a, const type a_b, const type a_alpha, const type a_beta){
			//in MKS unit...
			rho0  = a_rho0; // kg/m^3
			u0    = a_u0;   // J/kg
			uiv   = a_uiv;  // J/kg
			ucv   = a_ucv;  // J/kg
			A     = a_A;    // Pa
			B     = a_B;    // Pa
			a     = a_a;    // dimension-less
			b     = a_b;    //
			alpha = a_alpha;//
			beta  = a_beta; //
		}
		inline type Pressure(const type dens, const type eng) const{
			#if 1
			const type p_min = 1.0e+7;
			if(dens >= rho0 || eng < uiv){
				return std::max(P_co(dens, eng), p_min);
			}else if(dens < rho0 && eng > ucv){
				return std::max(P_ex(dens, eng), p_min);
			}else{
				return std::max(((eng - uiv) * P_ex(dens, eng) + (ucv - eng) * P_co(dens, eng)) / (ucv - uiv), p_min);
			}
			#else
			double p;
			if(dens >= rho0 || eng < uiv){
				p = P_co(dens, eng);
				if(dens <= 0.9 * rho0) return 1.0e-16;
			}else if(dens < rho0 && eng > ucv){
				p = P_ex(dens, eng);
			}else{
				p = ((eng - uiv) * P_ex(dens, eng) + (ucv - eng) * P_co(dens, eng)) / (ucv - uiv);
			}
			return (p > 1.0e+7) ? p : 1.0e+7;
			#endif
		}
		inline type SoundSpeed(const type dens, const type eng) const{
			if(dens <= 100.0) return 1.0e-8;
			return sqrt(std::max(Pressure(dens, eng) / (dens * dens) * dPdu(dens, eng) + dPdrho(dens, eng), 0.0) + 1.0e-16);
		}
	};
	template <typename real> class RankineHugoniot : public EoS_t<real>{
		const real p0;  //reference pressure
		const real K0;  //bulk moduls
		const real rho0;//reference density
		const real lmd; //Lambda
		public:
		RankineHugoniot(const real _p0, const real _K0, const real _rho0, const real _lmd) : p0(_p0), K0(_K0), rho0(_rho0), lmd(_lmd){
		}
		real Pressure(const double dens, const double eng) const{
			const real V0 = 1.0 / rho0;
			const real V  = 1.0 / dens;
			//const real vs = 1.0 - rho0 / dens;
			//return p0 + K0 * vs / pow(1. - lmd * vs, 2);
			return p0 + K0 / rho0 * (V0 - V) / pow(V0 - lmd * (V0 - V), 2);
		}
		real SoundSpeed(const double dens, const double eng) const{
			const real drho = 0.001 * dens;
			const real dPdrho = (Pressure(dens + drho, eng) - Pressure(dens - drho, eng)) / (2.0 * drho);
			return sqrt(dPdrho);
		}
	};
	struct HS{
		double Mm, sigma0, eta, zeta, rho0;
		int Natom, Nmol;
		HS(const double _Mm, const double _rho0, const double _sigma0, const double _eta, const double _zeta, const int _Natom): Mm(_Mm), rho0(_rho0), sigma0(_sigma0), eta(_eta), zeta(_zeta), Natom(_Natom), Nmol(1){
		}
		HS(){
		}
		double getHardSphereDiamiterViaDensityAndTemperature(const double dens, const double temp) const{
			return sigma0 * pow((temp + 1.0e-16) / 1673., eta) * pow(dens/rho0, -zeta/3.0);
		}
		virtual double getPackingFractionViaDensityAndTemperature(const double dens, const double temp) const{
			const double sigma = getHardSphereDiamiterViaDensityAndTemperature(dens, temp);
			return std::min(pi / 6.0 * pow(sigma, 3) * NA / Mm * Nmol * dens, 0.5);
			//return (pi / 6.0 * pow(sigma, 3) * NA / Mm * Nmol * dens);
		}
		virtual double excludeVolumeCorrection(const double pf) const{
			//Psi - 1.0 in my note.
			return (4.0 * pf - 2.0 * pf * pf + pf * pf * pf) / pow(1.0 - pf, 3);
		}
		virtual double excludeVolumeCorrection(const double dens, const double temp) const{
			//Psi - 1.0 in my note.
			const double y = getPackingFractionViaDensityAndTemperature(dens, temp);
			return excludeVolumeCorrection(y);
		}
		double getSpecificThermalEnergyViaDensityAndTemperature(const double dens, const double temp) const{
			return 3.0 * Rbar / Mm * Nmol * (0.5 + eta * excludeVolumeCorrection(dens, temp)) * temp;
		}
		double getTemperatureViaDensityAndSpecificThermalEnergy(const double dens, const double eng, const double tolerance = 1.0e-10) const{
			if(eta <= 0.0) return eng / (1.5 * Rbar / Mm * Nmol);
			
			double temp[3];
			temp[0] = 0.0;
			temp[1] = 1673.0;
			for(int i = 0 ; i < 10 ; ++ i){
				double diff_i   = eng - getSpecificThermalEnergyViaDensityAndTemperature(dens, temp[1]);
				double diff_im1 = eng - getSpecificThermalEnergyViaDensityAndTemperature(dens, temp[0]);
				temp[2] = temp[1] - diff_i * (temp[1] - temp[0]) / (diff_i - diff_im1);
				////////
				temp[0] = temp[1];
				temp[1] = temp[2];
				if(std::abs(temp[1] - temp[0]) / std::abs(temp[0]) < tolerance) break;
			}
			return temp[2];
			
		}
		double getPressureViaDensityAndTemperature(const double dens, const double temp) const{
			const double vol_ratio = pow(sigma0 / getHardSphereDiamiterViaDensityAndTemperature(dens, temp), 3);
			return Rbar / Mm * dens * temp * (double)(Nmol) * ((1.0 - zeta) * (excludeVolumeCorrection(dens, temp) + 1.0));
			//return Rbar / Mm * dens * temp * (double)(Nmol) * ((1.0 - zeta) * (excludeVolumeCorrection(dens, temp) + 1.0) - cbrt(dens / rho0) * (excludeVolumeCorrection(rho0, T0) + 1.0) + zeta * (excludeVolumeCorrection(rho0, T0) + 1.0) * pow(vol_ratio, 5.0 / 3.0));
			//return Rbar / Mm * dens * temp * (double)(Nmol) * ((1.0 - zeta) * (excludeVolumeCorrection(dens, temp) + 1.0) - T0 / temp * cbrt(dens / rho0) * (excludeVolumeCorrection(rho0, T0) + 1.0) + zeta * (excludeVolumeCorrection(rho0, T0) + 1.0) * pow(vol_ratio, 5.0 / 3.0));
		}
		double getPressureViaDensityAndSpecificThermalEnergy(const double dens, const double eng) const{
			const double temp = getTemperatureViaDensityAndSpecificThermalEnergy(dens, eng);
			return getPressureViaDensityAndTemperature(dens, temp);
		}
	};

	struct HSmix: public HS, EoS_t<double>{
		const HS comp0, comp1;
		const double frac0, frac1;
		//const int Nmol;
		HSmix(HS _a, int _n_a, HS _b, int _n_b): comp0(_a), frac0(1.0 * _n_a / (_n_a + _n_b)), comp1(_b), frac1(1.0 * _n_b / (_n_a + _n_b)){
			sigma0 = cbrt(frac0 * pow(comp0.sigma0, 3) + frac1 * pow(comp1.sigma0, 3));
			Mm = _n_a * comp0.Mm + _n_b * comp1.Mm;
			eta  = (comp0.eta * frac0 * pow(comp0.sigma0, 3) + comp1.eta * frac1 * pow(comp1.sigma0, 3))/(frac0 * pow(comp0.sigma0, 3) + frac1 * pow(comp1.sigma0, 3));
			zeta = (comp0.zeta * frac0 * pow(comp0.sigma0, 3) + comp1.zeta * frac1 * pow(comp1.sigma0, 3))/(frac0 * pow(comp0.sigma0, 3) + frac1 * pow(comp1.sigma0, 3));
			rho0 = comp0.rho0 * frac0 + comp1.rho0 * frac1;
			Natom = _n_a * comp0.Natom + _n_b * comp1.Natom;
			Nmol = _n_a + _n_b;
		}
		#if 0
		double getPackingFractionViaDensityAndTemperature(const double dens, const double temp) const{
			//???
			//return (frac0 * comp0.getPackingFractionViaDensityAndTemperature(dens, temp) + frac1 * comp1.getPackingFractionViaDensityAndTemperature(dens, temp));
		}
		#endif
		double excludeVolumeCorrection(const double pf, const double y1 = 0.0, const double y2 = 0.0) const{
			//Psi - 1.0 in my note.
			return ((4.0 - 3.0 * y1) * pf - (2.0 + 3.0 * y2) * pf * pf + pf * pf * pf) / pow(1.0 - pf, 3);
		}
		double excludeVolumeCorrection(const double dens, const double temp) const{
			//Psi - 1.0 in my note.
			const double pf      = getPackingFractionViaDensityAndTemperature(dens, temp);
			const double pf_0    = frac0 * comp0.getPackingFractionViaDensityAndTemperature(dens, temp);
			const double pf_1    = frac1 * comp1.getPackingFractionViaDensityAndTemperature(dens, temp);
			const double sigma_0 = comp0.getHardSphereDiamiterViaDensityAndTemperature(dens, temp);
			const double sigma_1 = comp1.getHardSphereDiamiterViaDensityAndTemperature(dens, temp);
			const double Delta   = sqrt(pf_0 * pf_1) / pf * pow(sigma_0 - sigma_1, 2) / (sigma_0 * sigma_1) * sqrt(frac0 * frac1);
			const double y1 = Delta * (sigma_0 + sigma_1) / sqrt(sigma_0 * sigma_1);
			const double y2 = Delta / pf * (pf_0 * sqrt(sigma_1 / sigma_0) + pf_1 * sqrt(sigma_0 / sigma_1));
			return excludeVolumeCorrection(pf, y1, y2);
		}
		double Pressure(const double dens, const double eng) const{
			return std::max(getPressureViaDensityAndSpecificThermalEnergy(dens, eng), 1.0e+7);
			//return (getPressureViaDensityAndSpecificThermalEnergy(dens, eng));
		}
		double SoundSpeed(const double dens, const double eng) const{
			#if 0
				const double eng_ = std::max(eng, 1.0e+3);
			#else
				const double eng_ = eng;
			#endif
			const double drho = 0.001 * dens;
			const double du   = 0.001 * eng_ + 1.0e-16;
			#if 0
				const double dPdrho = (getPressureViaDensityAndSpecificThermalEnergy(dens + drho, eng_) - getPressureViaDensityAndSpecificThermalEnergy(dens - drho, eng_)) / (2.0 * drho);
				const double dPdu   = (getPressureViaDensityAndSpecificThermalEnergy(dens, eng_ + du) - getPressureViaDensityAndSpecificThermalEnergy(dens, eng_ - du)) / (2.0 * du);
				return (sqrt(std::max(getPressureViaDensityAndSpecificThermalEnergy(dens, eng_) / (dens * dens) * dPdu + dPdrho, 0.0) + 1.0e-16));
			#else
				const double dPdrho = (Pressure(dens + drho, eng_) - Pressure(dens - drho, eng_)) / (2.0 * drho);
				const double dPdu   = (Pressure(dens, eng_ + du) - Pressure(dens, eng_ - du)) / (2.0 * du);
				//if(isnan(sqrt(std::max(Pressure(dens, eng_) / (dens * dens) * dPdu + dPdrho, 0.0) + 1.0e-16))) printf("EoS ER: %e, %e, %e, %e\n", dPdrho, dPdu, dens, eng);
				return (sqrt(std::max(Pressure(dens, eng_) / (dens * dens) * dPdu + dPdrho, 0.0) + 1.0e-16));
			#endif
		}
		double Temperature(const double dens, const double eng) const{
			return getTemperatureViaDensityAndSpecificThermalEnergy(dens, eng);
		}
	};

	struct BM : public EoS_t<double>{
		const int Natom;
		//Debye Temp. at reference state
		const double TD0;
		//Gruneisen param. at reference state.
		const double gmm0;
		//volume dependence of Gruneisen param.
		const double q;
		//reference density
		const double rho0;
		//Bulk modulus
		const double K0;
		//molar mass
		const double Mm;
		BM(const int _Natom, const double _TD0, const double _gmm0, const double _q, const double _rho0, const double _K0, const double _Mm): Natom(_Natom), TD0(_TD0), gmm0(_gmm0), q(_q), rho0(_rho0), K0(_K0), Mm(_Mm){
		}
		double getGruneisenParam(const double dens) const{
			return gmm0 * pow(dens / rho0, -q);
		}
		double getSpecificThermalEnergyViaDensityAndTemperature(const double dens, const double temp) const{
			//gruneisen param.
			const double gru = getGruneisenParam(dens);
			//beta hbar omega
			const double bho = cbrt(pi / 6.0) * (TD0 / temp) * exp((gmm0 - gru) / q);
			return 3.0 * Natom * Rbar / Mm * temp * bho * (0.5 + 1.0 / (exp(bho) - 1.0));
			//return 1.5 * Rbar / Mm * temp * bho * (1.0 + exp(-bho)) / (1.0 - exp(-bho)) * Natom;
		}
		double getPressureViaDensityAndSpecificThermalEnergy(const double dens, const double eng) const{
			return getGruneisenParam(dens) * eng * dens + 1.5 * K0 * (pow(dens / rho0, 7. / 3.) - pow(dens / rho0, 5. / 3.));
		}
		double getTemperatureViaDensityAndSpecificThermalEnergy(const double dens, const double eng, const double tolerance = 1.0e-10) const{
			double temp[3];
			temp[0] = TD0;
			temp[1] = 2.0 * TD0;
			for(int i = 0 ; i < 10 ; ++ i){
				double diff_i   = eng - getSpecificThermalEnergyViaDensityAndTemperature(dens, temp[1]);
				double diff_im1 = eng - getSpecificThermalEnergyViaDensityAndTemperature(dens, temp[0]);
				temp[2] = temp[1] - diff_i * (temp[1] - temp[0]) / (diff_i - diff_im1);
				////////
				temp[0] = temp[1];
				temp[1] = temp[2];
				if(std::abs(temp[1] - temp[0]) / std::abs(temp[0]) < tolerance) break;
			}
			return temp[2];
		}
		double getPressureViaDensityAndTemperature(const double dens, const double temp) const{
			const double eng = getSpecificThermalEnergyViaDensityAndTemperature(dens, temp);
			return getPressureViaDensityAndSpecificThermalEnergy(dens, eng);
		}
		double Pressure(const double dens, const double eng) const{
			//if(dens < 0.9 * rho0) return 1.0e-16;
			return std::max(getPressureViaDensityAndSpecificThermalEnergy(dens, eng), 1.0e+7);
		}
		double SoundSpeed(const double dens, const double eng) const{
			//if(dens < 0.9 * rho0) return sqrt(1.0e-16);
			const double drho = 0.001 * dens;
			const double du   = 0.001 * eng;
			#if 0
				const double dPdrho = (getPressureViaDensityAndSpecificThermalEnergy(dens + drho, eng) - getPressureViaDensityAndSpecificThermalEnergy(dens - drho, eng)) / (2.0 * drho);
				const double dPdu   = (getPressureViaDensityAndSpecificThermalEnergy(dens, eng + du) - getPressureViaDensityAndSpecificThermalEnergy(dens, eng - du)) / (2.0 * du);
				return (sqrt(std::max(getPressureViaDensityAndSpecificThermalEnergy(dens, eng) / (dens * dens) * dPdu + dPdrho, 0.0) + 1.0e-16));
			#else
				//const double dPdrho = (Pressure(dens + drho, eng) - Pressure(dens - drho, eng)) / (2.0 * drho);
				//const double dPdu   = (Pressure(dens, eng + du) - Pressure(dens, eng - du)) / (2.0 * du);
				//return (sqrt(std::max(Pressure(dens, eng) / (dens * dens) * dPdu + dPdrho, 0.0) + 1.0e-16));
				const double dPdrho = getGruneisenParam(dens) * eng * (1.0 - q) + 0.5 * K0 / rho0 * (7.0 * powf(dens / rho0, 4./3.) - 5.0 * powf(dens / rho0, 2./3.));
				const double dPdu   = getGruneisenParam(dens) * dens;
				return (sqrt(std::max(Pressure(dens, eng) / (dens * dens) * dPdu + dPdrho, 0.0) + 1.0e-16));
			#endif
		}
		double Temperature(const double dens, const double eng) const{
			return getTemperatureViaDensityAndSpecificThermalEnergy(dens, eng);
		}
	};
	//Nakajima & Stevenson
	struct NS : public EoS_t<double>{
		//reference density: rho_0
		const double dens0;
		//reference temperature: T_0
		const double T0;
		//bulk modulus: KT0
		const double K_T0;
		//derivative of bulk modulus: K'_T0
		const double Kp_T0;
		//heat capacity: C_V
		const double C_V;
		//referenge gruneisen param.: gmm0
		const double gmm0;
		//q
		const double q;
		//reference energy: E0
		const double u0;
		NS(const double _dens0, const double _T0, const double _K_T0, const double _Kp_T0, const double _C_V, const double _gmm0, const double _q, const double _u0): dens0(_dens0), T0(_T0), K_T0(_K_T0), Kp_T0(_Kp_T0), C_V(_C_V), gmm0(_gmm0), q(_q), u0(_u0){
		}
		double getGruneisenParam(const double dens) const{
			return gmm0 * pow(dens / dens0, -q);
		}
		double getF(const double dens) const{
			return 0.5 * (pow(dens / dens0, 2./3.) - 1.0);
		}
		double getTemperatureViaDensityAndSpecificThermalEnergy(const double dens, const double eng) const{
			const double f = getF(dens);
			const double a3 = 3.0 * (Kp_T0 - 4.0);
			const double gmm = getGruneisenParam(dens);
			//return T0 + (eng - u0 - 9.0 * K_T0 / dens0 * (0.5 * f * f + a3 * f * f * f / 6.0) - C_V * T0 / q * (gmm - gmm0)) / C_V;
			//return T0 + (eng - C_V * T0 / q * (gmm - gmm0)) / C_V;
			return eng / C_V;
		}
		double Temperature(const double dens, const double eng) const{
			return getTemperatureViaDensityAndSpecificThermalEnergy(dens, eng);
		}
		double getPressureViaDensityAndTemperature(const double dens, const double temp) const{
			const double f = getF(dens);
			const double a3 = 3.0 * (Kp_T0 - 4.0);
			const double gmm = getGruneisenParam(dens);
			return 3.0 * K_T0 * pow(1. + 2.0 * f, 2.5) * (f + 0.5 * a3 * f * f) + C_V * (temp - T0) * gmm * dens;
		}
		double getPressureViaDensityAndSpecificThermalEnergy(const double dens, const double eng) const{
			const double temp = getTemperatureViaDensityAndSpecificThermalEnergy(dens, eng);
			return getPressureViaDensityAndTemperature(dens, temp);
		}
		double getSpecificThermalEnergyViaDensityAndTemperature(const double dens, const double temp) const{
			const double f = getF(dens);
			const double a3 = 3.0 * (Kp_T0 - 4.0);
			const double gmm = getGruneisenParam(dens);
			//return u0 + 9.0 * K_T0 / dens0 * (0.5 * f * f + a3 / 6.0 * f * f * f) + C_V * (temp - T0) + C_V * T0 / q * (gmm - gmm0);
			//return C_V * (temp - T0) + C_V * T0 / q * (gmm - gmm0);
			return C_V * temp;
		}
		double Pressure(const double dens, const double eng) const{
			return std::max(getPressureViaDensityAndSpecificThermalEnergy(dens, eng), 1.0e+7);
		}
		double dPdrho(const double rho, const double u) const{
			const double drho = 0.0001;
			return (Pressure(rho + drho, u) - Pressure(rho - drho, u)) / (2.0 * drho);
		}
		double dPdu(const double rho, const double u) const{
			const double du = 0.0001;
			return (Pressure(rho, u + du) - Pressure(rho, u - du)) / (2.0 * du);
		}
		double SoundSpeed(const double dens, const double eng) const{
			return sqrt(std::max(Pressure(dens, eng) / (dens * dens) * dPdu(dens, eng) + dPdrho(dens, eng), 0.0) + 1.0e-16);
		}
	};


	struct SESAME_table: public EoS_t<double>{
		/*******************
			pres[temp_idx][dens_idx]
			and so on
		*******************/
		using vector2d = std::vector<std::vector<double> >;
		std::vector<double> dens;
		std::vector<double> temp;
		vector2d pres;//PRESsure
		vector2d ieng;//Internal ENerGy
		vector2d heng;//Helmholtz ENerGy
		vector2d entr;//ENTRopy
		vector2d sdsp;//SounD SPeed
		vector2d htcp;//HeaT CaPacity
		vector2d kpaf;//KPA Flag(?)
		public:
		/*******************
			Quantity of State
		*******************/
		struct QoS{
			double dens, temp, pres, ieng, heng, entr, sdsp, htcp, kpaf;
			QoS(){
				/*
				dens = 0.0/0.0;
				temp = 0.0/0.0;
				pres = 0.0/0.0;
				ieng = 0.0/0.0;
				heng = 0.0/0.0;
				entr = 0.0/0.0;
				sdsp = 0.0/0.0;
				htcp = 0.0/0.0;
				*/
				dens = 0.0;
				temp = 0.0;
				pres = 0.0;
				ieng = 0.0;
				heng = 0.0;
				entr = 0.0;
				sdsp = 1.0e-16;
				htcp = 0.0;
				kpaf = -1.0;
			}
		};
		SESAME_table(){
			readTable("example/NEW-SESAME-STD.TXT", {&pres, &ieng, &heng});
			readTable("example/NEW-SESAME-EXT.TXT", {&entr, &sdsp, &htcp, &kpaf});
			changeUnit();
		}
		void changeUnit(){
			for(auto dens_itr = std::begin(dens) ; dens_itr != std::end(dens) ; ++ dens_itr){
				// g/cm3 -> kg/m3
				*dens_itr *= 1.0e+3;
			}
			for(int idx_temp = 0 ; idx_temp < temp.size() ; ++ idx_temp){
				for(int idx_dens = 0 ; idx_dens < dens.size() ; ++ idx_dens){
					// GPa -> Pa
					pres[idx_temp][idx_dens] *= 1.0e+9;
					// MJ/kg -> J/kg
					ieng[idx_temp][idx_dens] *= 1.0e+6;
					heng[idx_temp][idx_dens] *= 1.0e+6;
					// MJ/kg/K -> J/kg/K
					entr[idx_temp][idx_dens] *= 1.0e+6;
					htcp[idx_temp][idx_dens] *= 1.0e+6;
					// cm/s
					sdsp[idx_temp][idx_dens] *= 1.0e-2;
				}
			}
		}
		QoS calculate(const double dens_in, const double ieng_in) const{
			QoS ret;
			ret.dens = dens_in;
			ret.ieng = ieng_in;
			/*********************
				eng
				 ^
				 |  (low, hi )      (hi , hi )
				 |
				 |
				 |
				 |  (low, low)      (hi , low)
				 |
				 +------------------> dens
			**********************/
			struct{
				double dens, ieng, temp, pres, sdsp, heng, entr, htcp, kpaf;
			}ll, lh, hl, hh;

			//test density input
			if(dens_in < dens[0]){
				//std::cout << "Density undershoot" << std::endl;
				return ret;
				//exit(0);
			}else if(dens_in > dens[dens.size() - 1]){
				//std::cout << "Density overshoot" << std::endl;
				return ret;
				//exit(0);
			}
			//linier search for density
			int idx_dens;
			for(idx_dens = 0 ; idx_dens < dens.size() - 1 ; ++ idx_dens){
				if(dens[idx_dens] < dens_in && dens_in <= dens[idx_dens + 1]) break;
			}

			ll.dens = dens[idx_dens];
			lh.dens = dens[idx_dens];
			hl.dens = dens[idx_dens + 1];
			hh.dens = dens[idx_dens + 1];

			//test ieng input
			if(ieng_in < ieng[0][idx_dens] || ieng_in < ieng[0][idx_dens + 1]){
				//std::cout << "Internal energy undershoot" << std::endl;
				return ret;
				//exit(0);
			}else if(ieng_in > ieng[temp.size() - 1][idx_dens] || ieng_in > ieng[temp.size() - 1][idx_dens + 1]){
				//std::cout << "Internal energy overshoot" << ieng[temp.size() - 1][idx_dens] << " || " << ieng[temp.size() - 1][idx_dens + 1] << std::endl;
				return ret;
				//exit(0);
			}

			//find temperature index and store data
			int idx_temp;
			for(idx_temp = 0 ; idx_temp < temp.size() - 1 ; ++ idx_temp){
				if(ieng[idx_temp][idx_dens] <= ieng_in && ieng_in < ieng[idx_temp + 1][idx_dens]) break;
			}
			ll.ieng = ieng[idx_temp][idx_dens];
			lh.ieng = ieng[idx_temp + 1][idx_dens];
			ll.temp = temp[idx_temp];
			lh.temp = temp[idx_temp + 1];
			ll.pres = pres[idx_temp][idx_dens];
			lh.pres = pres[idx_temp + 1][idx_dens];
			ll.sdsp = sdsp[idx_temp][idx_dens];
			lh.sdsp = sdsp[idx_temp + 1][idx_dens];
			ll.heng = heng[idx_temp][idx_dens];
			lh.heng = heng[idx_temp + 1][idx_dens];
			ll.entr = entr[idx_temp][idx_dens];
			lh.entr = entr[idx_temp + 1][idx_dens];

			for(idx_temp = 0 ; idx_temp < temp.size() - 1 ; ++ idx_temp){
				if(ieng[idx_temp][idx_dens + 1] <= ieng_in && ieng_in < ieng[idx_temp + 1][idx_dens + 1]) break;
			}
			hl.ieng = ieng[idx_temp][idx_dens + 1];
			hh.ieng = ieng[idx_temp + 1][idx_dens + 1];
			hl.temp = temp[idx_temp];
			hh.temp = temp[idx_temp + 1];
			hl.pres = pres[idx_temp][idx_dens + 1];
			hh.pres = pres[idx_temp + 1][idx_dens + 1];
			hl.sdsp = sdsp[idx_temp][idx_dens + 1];
			hh.sdsp = sdsp[idx_temp + 1][idx_dens + 1];
			hl.heng = heng[idx_temp][idx_dens + 1];
			hh.heng = heng[idx_temp + 1][idx_dens + 1];
			hl.entr = entr[idx_temp][idx_dens + 1];
			hh.entr = entr[idx_temp + 1][idx_dens + 1];

			//bilinear interpol.
			//create interpolated data along ieng direction for both low and high densities
			const double temp_low_dens = ((lh.ieng - ieng_in) * ll.temp + (ieng_in - ll.ieng) * lh.temp) / (lh.ieng - ll.ieng);
			const double temp_hi_dens = ((hh.ieng - ieng_in) * hl.temp + (ieng_in - hl.ieng) * hh.temp) / (hh.ieng - hl.ieng);
			const double temp = ((hl.dens - dens_in) * temp_low_dens + (dens_in - ll.dens) * temp_hi_dens) / (hl.dens - ll.dens);

			const double pres_low_dens = ((lh.ieng - ieng_in) * ll.pres + (ieng_in - ll.ieng) * lh.pres) / (lh.ieng - ll.ieng);
			const double pres_hi_dens = ((hh.ieng - ieng_in) * hl.pres + (ieng_in - hl.ieng) * hh.pres) / (hh.ieng - hl.ieng);
			const double pres = ((hl.dens - dens_in) * pres_low_dens + (dens_in - ll.dens) * pres_hi_dens) / (hl.dens - ll.dens);

			const double sdsp_low_dens = ((lh.ieng - ieng_in) * ll.sdsp + (ieng_in - ll.ieng) * lh.sdsp) / (lh.ieng - ll.ieng);
			const double sdsp_hi_dens = ((hh.ieng - ieng_in) * hl.sdsp + (ieng_in - hl.ieng) * hh.sdsp) / (hh.ieng - hl.ieng);
			const double sdsp = ((hl.dens - dens_in) * sdsp_low_dens + (dens_in - ll.dens) * sdsp_hi_dens) / (hl.dens - ll.dens);

			const double heng_low_dens = ((lh.ieng - ieng_in) * ll.heng + (ieng_in - ll.ieng) * lh.heng) / (lh.ieng - ll.ieng);
			const double heng_hi_dens = ((hh.ieng - ieng_in) * hl.heng + (ieng_in - hl.ieng) * hh.heng) / (hh.ieng - hl.ieng);
			const double heng = ((hl.dens - dens_in) * heng_low_dens + (dens_in - ll.dens) * heng_hi_dens) / (hl.dens - ll.dens);

			const double entr_low_dens = ((lh.ieng - ieng_in) * ll.entr + (ieng_in - ll.ieng) * lh.entr) / (lh.ieng - ll.ieng);
			const double entr_hi_dens = ((hh.ieng - ieng_in) * hl.entr + (ieng_in - hl.ieng) * hh.entr) / (hh.ieng - hl.ieng);
			const double entr = ((hl.dens - dens_in) * entr_low_dens + (dens_in - ll.dens) * entr_hi_dens) / (hl.dens - ll.dens);

			ret.pres = std::max(pres, 1.0e+7);
			ret.heng = heng;
			ret.entr = entr;
			ret.temp = temp;
			ret.sdsp = sdsp;
			//std::cout << dens_in << " " << temp << " " << pres << " " << ieng_in << " " << heng << " " << entr << " " << sdsp << " " << 0.0 << " " << 0.0 << std::endl;

			return ret;
		}
		double Pressure(const double dens_in, const double ieng_in) const{
			const QoS data = calculate(dens_in, ieng_in);
			return data.pres;
		}
		double SoundSpeed(const double dens_in, const double ieng_in) const{
			const QoS data = calculate(dens_in, ieng_in);
			return data.sdsp;
		}
		double Temperature(const double dens_in, const double ieng_in) const{
			const QoS data = calculate(dens_in, ieng_in);
			return data.temp;
		}
		void readTable(const std::string file, const std::initializer_list<vector2d*> data){
			double Ndens, Ntemp;
			std::ifstream ifs(file);
			std::string buf;
			//skip the first 6 lines
			std::getline(ifs, buf);
			std::getline(ifs, buf);
			std::getline(ifs, buf);
			std::getline(ifs, buf);
			std::getline(ifs, buf);
			std::getline(ifs, buf);
			//get # of density points and temperature points
			ifs >> Ndens;
			ifs >> Ntemp;
			dens.resize(Ndens);
			temp.resize(Ntemp);
			//read density data
			for(int idx_dens = 0 ; idx_dens < Ndens ; ++ idx_dens){
				double dens_buf;
				ifs >> dens_buf;
				dens[idx_dens] = dens_buf;
			}
			//read temp. data
			for(int idx_temp = 0 ; idx_temp < Ntemp ; ++ idx_temp){
				double temp_buf;
				ifs >> temp_buf;
				temp[idx_temp] = temp_buf;
			}
			for(auto itr = std::begin(data) ; itr != std::end(data) ; ++ itr){
				(*itr)->resize(Ntemp);
				for(int idx_temp = 0 ; idx_temp < Ntemp ; ++ idx_temp){
					(**itr)[idx_temp].resize(Ndens);
					for(int idx_dens = 0 ; idx_dens < Ndens ; ++ idx_dens){
						double data_buf;
						ifs >> data_buf;
						(**itr)[idx_temp][idx_dens] = data_buf;
					}
				}
			}
			ifs.close();
		}
		void dumpTable(){
			////////////////////
			//dump
			////////////////////
			for(int i = 0 ; i < dens.size() ; ++ i){
				for(int j = 0 ; j < temp.size() ; ++ j){
					std::cout << dens[i] << " " << temp[j] << " " << pres[j][i] << " " << ieng[j][i] << " " << heng[j][i] << " " << entr[j][i] << " " << sdsp[j][i] << " " << htcp[j][i] << " " << kpaf[j][i] << std::endl;
				}
			}
		}
	};

	struct Wissing: public EoS_t<double>{
		template <typename real> real ExpInt(const real n, const real x, const std::size_t N = 128) const{
			real ret = 0.0;
			for(std::size_t i = 1 ; i < N ; ++ i){
				const real t = 1.0 / N * i;
				const real t_up = t + 1.0 / N;
				ret = ret + (powf(t, n - 2) * exp(- x / t) + powf(t_up, n - 2) * exp(- x / t_up)) * 1.0 / N * 0.5;
			}
			return ret;
		}
		const double rho0;
		const double B0;
		const double B0p;
		const double t_min;
		const double delta_t;
		const double a_exp;
		const double c_exp;
		const double gmm0;
		const double q0;
		Wissing(const double rho0, const double B0, const double B0p, const double t_min, const double delta_t, const double a_exp , const double c_exp, const double gmm0, const double q0): rho0(rho0), B0(B0), B0p(B0p), t_min(t_min), delta_t(delta_t), a_exp(a_exp), c_exp(c_exp), gmm0(gmm0), q0(q0){
		}
		double P_co(const double dens, const double eng) const{
			const double A2 = 1.95;
			const double A0 = B0p - A2;
			const double A1 = B0p / A0;
			const double eta = dens / rho0;
			const double p_co = B0 * exp(A0 / A1) / A1 * (powf(eta, A2) * ExpInt(1 + A2 / A1, A0 / A1 * powf(eta, - A1)) - ExpInt(1 + A2 / A1, A0 / A1));
			const double B_co = B0 * powf(eta, A2) * exp(A0 / A1 * (1.0 - powf(eta, - A1)));
			const double Bp_co = A0 * powf(eta, - A1) + A2;

			const double t_inf = 2.5;
			const double t = (t_min - t_inf) * powf(eta, - delta_t) + t_inf;
			const double rho_dt_drho = - (t_min - t_inf) * powf(eta, - delta_t);
			const double gmm = (0.5 * Bp_co - 1.0 / 6.0 - t / 3.0 * (1.0 - p_co / (3.0 * B_co)) - p_co / (3.0 * B_co) * rho_dt_drho) / (1.0 - 2.0 * t / 3.0 * p_co / B_co);
			const double p_th = gmm * dens * eng;

			return p_co + p_th;
		}
		double P_ex(const double dens, const double eng) const{
			const double eta = dens / rho0;
			const double b_exp = 5.0/3.0;
			const double p_co = B0 / (a_exp - b_exp) * (powf(eta, a_exp) - powf(eta, b_exp));
			const double gmm_ideal = 1.0/3.0;
			const double gmm = (1.0 + gmm0 - gmm_ideal) * powf(eta, c_exp) - powf(eta, q0 * gmm0 + c_exp * (1.0 + gmm0 - gmm_ideal)) + gmm_ideal;
			const double p_th = gmm * dens * eng;

			return p_co + p_th;
		}
		double dPdrho(const double rho, const double u) const{
			const double drho = 0.0001 * rho0;
			return (Pressure(rho + drho, u) - Pressure(rho - drho, u)) / (2.0 * drho);
		}
		double dPdu(const double rho, const double u) const{
			const double du = 0.0001;
			return (Pressure(rho, u + du) - Pressure(rho, u - du)) / (2.0 * du);
		}
		double Pressure(const double dens, const double eng) const{
			const double p_min = 1.0e+7;
			return (dens > rho0) ? std::max(P_co(dens, eng), p_min) : std::max(P_ex(dens, eng), p_min);
		}
		double SoundSpeed(const double dens, const double eng) const{
			return sqrt(std::max(Pressure(dens, eng) / (dens * dens) * dPdu(dens, eng) + dPdrho(dens, eng), 0.0) + 1.0e-16);
			//return sqrt(  dPdrho(dens, eng) );
			//return 0;
		}
	};
}
//Note: 
//Dunite  -> Solid Mg2SiO4;
//Olivine -> Liquid
//static const EoS::IdealGas<PS::F64>  Monoatomic(5./3.);
//static const EoS::IdealGas<PS::F64>  Diatomic  (1.4);
//static const EoS::Tillotson<PS::F64> Granite   (2680.0,  16.0e+6, 3.500e+6, 18.00e+6,  18.00e+9,  18.00e+9, 0.5, 1.3, 5.0, 5.0);
//static const EoS::Tillotson<PS::F64> Basalt    (2700.0, 487.0e+6, 4.720e+6, 18.20e+6,  26.70e+9,  26.70e+9, 0.5, 1.5, 5.0, 5.0);

static const EoS::Tillotson<PS::F64> Dunite    (3500.0, 550.0e+6, 4.500e+6, 14.50e+6, 131.00e+9,  49.00e+9, 0.5, 1.4, 5.0, 5.0);
//static const EoS::BM Dunite(7, 676.0, 1.5, 1.0, 3500.0, 131.0e+9, 140.0e-3);
//static const EoS::NS Dunite(3680.0, 2000.0, 200.e+9, 4.14, 1200.0, 1.0, 1.0, 1.995e+6);
//static const EoS::SESAME_table Dunite;
//static const EoS::Wissing Dunite(3300.0, 130e+9, 4.2, 2.2, 13.0, 5.924, 0.770, 1.2, 2.25);

static const EoS::Tillotson<PS::F64> Iron      (7800.0,   9.5e+6, 2.400e+6,  8.67e+6, 128.00e+9, 105.00e+9, 0.5, 1.5, 5.0, 5.0);
//static const EoS::BM Iron  (1, 470.0, 1.5, 1.0, 7800.0, 128.0e+9,  55.8e-3);

//static const EoS::Tillotson<PS::F64> Water     ( 998.0,   7.0e+6, 0.419e+6,  2.69e+6,   2.18e+9,   2.18e+9, 0.7, 0.15, 10.0, 5.0);
//static const EoS::Tillotson<PS::F64> Water     ( 998.0,   7.0e+6, 0.419e+6,  2.69e+6,   2.18e+9,  13.25e+9, 0.7, 0.15, 10.0, 5.0);
//static const EoS::Tillotson<PS::F64> WetTuff   (1970.0,  11.0e+6, 3.200e+6, 16.00e+6,  10.00e+9,   6.00e+9, 0.5, 1.30,  5.0, 5.0);

//static const EoS::HSmix Olivine(EoS::HS(60.0e-3, 2650.0, 0.3352e-9, -0.03, 0.62, 3), 1, EoS::HS(40.0e-3, 3580.0, 0.2607e-9,  0.08, 0.22, 2), 2);
static const EoS::HSmix Olivine(EoS::HS(60.0e-3, 2650.0, 0.3352e-9, 0.0, 0.0, 3), 1, EoS::HS(40.0e-3, 3580.0, 0.2607e-9, 0.0, 0.0, 2), 2);
//static const EoS::NS Olivine(2650.0, 2000.0, 27.3e+9, 5.71, 1480.0, 0.6, -1.6, 2.64e+6);
//static const EoS::Wissing Olivine(2800.0, 23e+9, 6.2, 2.2, 0.0, 5.924, 0.770, 2.2, -0.17);

//static const EoS::RankineHugoniot<PS::F64> Melt_ ( 0.0e+9,  60.0e+9, 3000.0, 1.3);
//static const EoS::RankineHugoniot<PS::F64> Solid_( 0.0e+9, 160.0e+9, 3500.0, 1.3);

